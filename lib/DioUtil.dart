import 'package:dio/dio.dart';

class DioUtil {
static String ImageUrl = "";
 static Dio createDio(){
    BaseOptions _baseOptions = BaseOptions(
        receiveTimeout: 30000,
        connectTimeout: 11000, //receiveDataWhenStatusError: true,
        followRedirects: false,
        validateStatus: (status) {return status > 0;} , baseUrl: "http://172.104.152.104:8080/api/");

   Dio _dio = Dio(_baseOptions);
  // _dio.interceptors.add(AuthInterceptor());
    return _dio;
  }
}
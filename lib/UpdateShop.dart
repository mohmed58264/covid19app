import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mwaedy_app/Dialogs.dart';
import 'package:mwaedy_app/DioUtil.dart';
import 'package:mwaedy_app/Shops.dart';

class UpdateShop extends StatefulWidget {
  String username;

  UpdateShop(this.username);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<UpdateShop> {
  bool progressSave = false;
  bool progress = true;
  Shops shop;
  TextEditingController txtShopName = new TextEditingController();
  TextEditingController txtAllCapacity = new TextEditingController();
  TextEditingController txtUserName = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController txtimage = new TextEditingController();
  TextEditingController txtCurrentCapacity = new TextEditingController();
  int _value = 1;
  String _value2 = "Extreme";

  @override
  void initState() {
    _getData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    //  key: Dialogs.ScaffoldStates,
      body: Center(
        child: ListView(
          children: <Widget>[
            Container(
              child: Image(
                image: new AssetImage('assets/images/logo.png'),
                width: 150,
                height: 150,
              ),
              margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
            ),

            Card(
                child: Column(
                  children: <Widget>[

                    Container(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: SizedBox(
                          child: Text(
                            "Shop Name ",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(right: 20, top: 20),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(
                            controller: txtShopName,
                            decoration: new InputDecoration(
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2.0),
                              ),
                              border: const OutlineInputBorder(),
                              //    hintText: 'mohmed',
                              labelStyle: TextStyle(color: Colors.brown),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),



                    Container(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: SizedBox(
                          child: Text(
                            "Image Link",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(right: 20, top: 20),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(
                            controller: txtimage,
                            decoration: new InputDecoration(
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2.0),
                              ),
                              border: const OutlineInputBorder(),
                              //    hintText: 'mohmed',
                              labelStyle: TextStyle(color: Colors.brown),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),


                    Container(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: SizedBox(
                          child: Text(
                            "User Name",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(right: 20, top: 2),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(enabled: false,
                            controller: txtUserName,
                            keyboardType: TextInputType.emailAddress,
                            decoration: new InputDecoration(
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2.0),
                              ),
                              border: const OutlineInputBorder(),
                              //    hintText: 'mohmed',
                              labelStyle: TextStyle(color: Colors.brown),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),


                    Container(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: SizedBox(
                          child: Text(
                            " Password ",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(right: 20, top: 2),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(
                            controller: password,
                            obscureText: true,
                            decoration: new InputDecoration(
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2.0),
                              ),
                              border: const OutlineInputBorder(),
                              labelStyle: TextStyle(color: Colors.brown),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),


                    Container(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: SizedBox(
                          child: Text(
                            " Statues ",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(right: 20, top: 2),
                    ),
                    Container(
                      width: 300,
                      child: DropdownButton(
                          icon: Icon(null),
                          iconSize: 30,
                          value: _value2,
                          items: [
                            DropdownMenuItem(child: Text("Extreme"), value: "Extreme"),
                            DropdownMenuItem(child: Text("High"), value: "High"),
                            DropdownMenuItem(child: Text("Medium"), value: "Medium"),
                            DropdownMenuItem(child: Text("Low"), value: "Low"),
                          ],
                          onChanged: (value) {
                            setState(() {
                              _value2 = value;
                            });
                          }),
                    ),


                    Container(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: SizedBox(
                          child: Text(
                            " Current Capacity ",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(right: 20, top: 2),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(
                            controller: txtCurrentCapacity,
                            decoration: new InputDecoration(
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2.0),
                              ),
                              border: const OutlineInputBorder(),
                              labelStyle: TextStyle(color: Colors.brown),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),


                    Container(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: SizedBox(
                          child: Text(
                            " Total capacity ",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(right: 20, top: 2),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(keyboardType : TextInputType.number,
                            controller: txtAllCapacity,
                            decoration: new InputDecoration(
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2.0),
                              ),
                              border: const OutlineInputBorder(),
                              //    hintText: 'mohmed',
                              labelStyle: TextStyle(color: Colors.brown),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),


                    Container(
                      child: SizedBox(
                        child: RaisedButton(
                            child: progressSave
                                ? Center(
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                              ),
                            )
                                : Text(
                              "Capacitance change",
                              style: TextStyle(
                                  fontSize: 14, color: Colors.white),
                            ),
                            padding: EdgeInsets.fromLTRB(89, 0, 89, 0),
                            onPressed: _onUpdatePressed,
                            color: Theme.of(context).primaryColor,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10.0))),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 50),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }

  _onUpdatePressed() async {
    setState(() {
      progressSave = true;
    });

    DioUtil.createDio()
        .post("Main/Update",
        data: new Shops(
            allCapacity: int.parse(txtAllCapacity.text),
            currentCapacity: int.parse(txtCurrentCapacity.text),
            id: shop.id,
            password: password.text,
            shopName: txtShopName.text,shopImage: txtimage.text,
            type: shop.type,
            statuse: _value2,
            username: txtUserName.text)
            .toJson())
        .then((response) {
      if (response.statusCode == 200) {
        print(response.data);
        if (response.data == "1") {
          setState(() {
            Dialogs.okAlter(context, 'Update', 'Update Done');
            progressSave =false;
          });
        } else {
          Dialogs.okAlter(
              context, 'Update', 'Error Model');
          setState(() {
            print(response.statusCode);
            print(response.data.toString());
            progressSave = false;
          });
        }
      } else {
        setState(() {
          progressSave = false;
        });
        print(
            'Error*Case********************************************************' +
                response.statusCode.toString());
        setState(() {});
      }
    }).catchError((error) {
      setState(() {
        progressSave = false;
      });

      print('Error*********************************************************' +
          error.toString());
    });
  }



  void _getData() {
    DioUtil.createDio()
        .get("Main/GetData?username=" + widget.username)
        .then((response) {
      if (response.statusCode == 200) {
         shop = Shops.fromJson(response.data);
        setState(() {
          progress = false;
          txtShopName.text = shop.shopName;
          txtAllCapacity.text = shop.allCapacity.toString();
          txtUserName.text = shop.username;
          password.text = shop.password;
          txtimage.text = shop.shopImage;
          txtCurrentCapacity.text = shop.currentCapacity.toString();
        });
      } else {
        setState(() {
          progress = false;
        });
        print(
            'Error*Case********************************************************' +
                response.statusCode.toString());
      }
    }).catchError((error) {
      setState(() {
        progress = false;
      });

      print('Error*********************************************************' +
          error.toString());
    });
  }


}

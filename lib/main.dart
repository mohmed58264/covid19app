import 'package:flutter/material.dart';
import 'package:mwaedy_app/AdminOrUser.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  Color color =
  const Color(0xff737afd);



  // Second `const` is optional in assignments.
  Color accentColor =
  const Color(0xfff9467e); // Second `const` is optional in assignments.


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.lightBlue,
          accentColor: accentColor,
          primaryColor: color,
          buttonColor: Colors.lightBlue,
          primaryTextTheme: TextTheme(button: TextStyle(color: Colors.red)),
          fontFamily: 'cocon'),
      home: AdminOrUser(),
      locale: Locale('ar'),
    );
  }


}
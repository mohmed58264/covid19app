import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:mwaedy_app/Dialogs.dart';
import 'package:mwaedy_app/DioUtil.dart';
import 'package:mwaedy_app/Shops.dart';

class Detailes extends StatefulWidget {
  String username;

  Detailes(this.username);

  @override
  _DetailesState createState() => _DetailesState();
}

class _DetailesState extends State<Detailes> {
  bool progressSave = false;
  bool progress = true;
  Shops shop;
  TextEditingController txtShopName = new TextEditingController();
  TextEditingController txtAllCapacity = new TextEditingController();
  TextEditingController txtUserName = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController txtimage = new TextEditingController();
  TextEditingController txtState = new TextEditingController();
  TextEditingController txtCurrent = new TextEditingController();
  int _value = 1;

  @override
  void initState() {
    _getData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //  key: Dialogs.ScaffoldStates,
      body:progress?Center(): Center(
        child: ListView(
          children: <Widget>[
        Container(width: 20,
        child: Image.network(shop.shopImage,
            height: 150,
            width: 90,
            loadingBuilder: (BuildContext context, Widget child,
                ImageChunkEvent loadingProgress) {
              if (loadingProgress == null) return child;
              return Center(
                child: CircularProgressIndicator(
                  value: loadingProgress.expectedTotalBytes !=
                      null
                      ? loadingProgress.cumulativeBytesLoaded /
                      loadingProgress.expectedTotalBytes
                      : null,
                ),
              );
            }
        ),
    ),

            Card(
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: SizedBox(
                          child: Text(
                            "Shop name",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(right: 20, top: 20),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(enabled: false,
                            controller: txtShopName,
                            decoration: new InputDecoration(
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2.0),
                              ),
                              border: const OutlineInputBorder(),
                              //    hintText: 'mohmed',
                              labelStyle: TextStyle(color: Colors.brown),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),


                    Container(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: SizedBox(
                          child: Text(
                            " Total capacity ",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(right: 20, top: 2),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(keyboardType : TextInputType.number,enabled: false,
                            controller: txtAllCapacity,
                            decoration: new InputDecoration(
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2.0),
                              ),
                              border: const OutlineInputBorder(),
                              //    hintText: 'mohmed',
                              labelStyle: TextStyle(color: Colors.brown),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),


                    Container(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: SizedBox(
                          child: Text(
                            " Statues ",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(right: 20, top: 2),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(keyboardType : TextInputType.number,enabled: false,
                            controller: txtState,
                            decoration: new InputDecoration(
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2.0),
                              ),
                              border: const OutlineInputBorder(),
                              //    hintText: 'mohmed',
                              labelStyle: TextStyle(color: Colors.brown),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),


                    Container(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: SizedBox(
                          child: Text(
                            "Current capacity ",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(right: 20, top: 2),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(enabled: false,
                            controller: txtCurrent,
                            keyboardType: TextInputType.emailAddress,
                            decoration: new InputDecoration(
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2.0),
                              ),
                              border: const OutlineInputBorder(),
                              //    hintText: 'mohmed',
                              labelStyle: TextStyle(color: Colors.brown),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),

            /*        Container(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: SizedBox(
                          child: Text(
                            "  Used capacity ",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(right: 20, top: 2),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                      child: SizedBox(
                        child: TextField(enabled: false,
                            controller: password,
                            decoration: new InputDecoration(
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 2.0),
                              ),
                              border: const OutlineInputBorder(),
                              labelStyle: TextStyle(color: Colors.brown),
                            )),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    ),*/
                      SizedBox(child: RatingBar.builder(
  initialRating: 3,
  minRating: 1,
  direction: Axis.horizontal,
  allowHalfRating: true,
  itemCount: 5,
  itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
  itemBuilder: (context, _) => Icon(
    Icons.star,
    color: Colors.amber,
  ),
  onRatingUpdate: (rating) {
    print(rating);
  },
),),



                     Container(
                      child: SizedBox(
                        child: RaisedButton(
                            child: progressSave
                                ? Center(
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                              ),
                            )
                                : Text(
                              " Back",
                              style: TextStyle(
                                  fontSize: 14, color: Colors.white),
                            ),
                            padding: EdgeInsets.fromLTRB(89, 0, 89, 0),
                            onPressed: ()=>{Navigator.pop(context)},
                            color: Theme.of(context).primaryColor,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10.0))),
                        height: 50,
                      ),
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 50),
                    )
      ],
                ))
          ],
        ),
      ),
    );
  }

  _onRegisterPressed() async {
    setState(() {
      progressSave = true;
    });

    DioUtil.createDio()
        .post("Main/Update",
        data: new Shops(
            allCapacity: shop.allCapacity,
            currentCapacity: shop.currentCapacity - 1,
            id: shop.id,
            password: shop.password,
            shopName: shop.shopName,shopImage: shop.shopImage,
            type: shop.type,
            username: shop.username)
            .toJson())
        .then((response) {
      if (response.statusCode == 200) {
        print(response.data);
        if (response.data == "1") {
          setState(() {
            Navigator.pop(context);
            Dialogs.okAlter(context, 'Access', ' Access Done ');
            progressSave =false;
          });
        } else {

          setState(() {
            print(response.statusCode);
            print(response.data.toString());
            progressSave = false;
          });
        }
      } else {
        setState(() {
          progressSave = false;
        });
        print(
            'Error*Case********************************************************' +
                response.statusCode.toString());
        setState(() {});
      }
    }).catchError((error) {
      setState(() {
        progressSave = false;
      });

      print('Error*********************************************************' +
          error.toString());
    });
  }



  void _getData() {



    DioUtil.createDio()
        .get("Main/GetData?username=" + widget.username)
        .then((response) {
      if (response.statusCode == 200) {
        shop = Shops.fromJson(response.data);
        setState(() {
          progress = false;
          txtState.text = shop.statuse;
          txtShopName.text = shop.shopName;
          txtAllCapacity.text = shop.allCapacity.toString();
          txtCurrent.text = shop.currentCapacity.toString();
          password.text = (shop.allCapacity - shop.currentCapacity).toString();
        });
      } else {
        setState(() {
          progress = false;
        });
        print(
            'Error*Case********************************************************' +
                response.statusCode.toString());
      }
    }).catchError((error) {
      setState(() {
        progress = false;
      });

      print('Error*********************************************************' +
          error.toString());
    });
  }


}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mwaedy_app/Dialogs.dart';
import 'package:mwaedy_app/DioUtil.dart';
import 'package:mwaedy_app/Shops.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  bool progressSave = false;

  TextEditingController txtShopName = new TextEditingController();
  TextEditingController txtAllCapacity = new TextEditingController();
  TextEditingController txtUserName = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController txtimage = new TextEditingController();
  int _value = 1;
  String _value2 = "Extreme";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ListView(
          children: <Widget>[
            Container(
              child: Image(
                image: new AssetImage('assets/images/logo.png'),
                width: 150,
                height: 150,
              ),
              margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
            ),

            Card(
                child: Column(
              children: <Widget>[
                Container(
                  child: Align(
                    alignment: Alignment.topRight,
                    child: SizedBox(
                      child: Text(
                        "Shop name",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ),
                  margin: EdgeInsets.only(right: 20, top: 20),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                  child: SizedBox(
                    child: TextField(
                        controller: txtShopName,
                        decoration: new InputDecoration(
                          enabledBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 2.0),
                          ),
                          border: const OutlineInputBorder(),
                          //    hintText: 'mohmed',
                          labelStyle: TextStyle(color: Colors.brown),
                        )),
                    height: 50,
                  ),
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                ),

                Container(
                  child: Align(
                    alignment: Alignment.topRight,
                    child: SizedBox(
                      child: Text(
                        "Image Link",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ),
                  margin: EdgeInsets.only(right: 20, top: 20),
                ),

                Container(
                  padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                  child: SizedBox(
                    child: TextField(
                        controller: txtimage,
                        decoration: new InputDecoration(
                          enabledBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 2.0),
                          ),
                          border: const OutlineInputBorder(),
                          //    hintText: 'mohmed',
                          labelStyle: TextStyle(color: Colors.brown),
                        )),
                    height: 50,
                  ),
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                ),



                Container(
                  child: Align(
                    alignment: Alignment.topRight,
                    child: SizedBox(
                      child: Text(
                        "Username",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ),
                  margin: EdgeInsets.only(right: 20, top: 2),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                  child: SizedBox(
                    child: TextField(
                        controller: txtUserName,
                        keyboardType: TextInputType.emailAddress,
                        decoration: new InputDecoration(
                          enabledBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 2.0),
                          ),
                          border: const OutlineInputBorder(),
                          //    hintText: 'mohmed',
                          labelStyle: TextStyle(color: Colors.brown),
                        )),
                    height: 50,
                  ),
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                ),
                Container(
                  child: Align(
                    alignment: Alignment.topRight,
                    child: SizedBox(
                      child: Text(
                        " password",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ),
                  margin: EdgeInsets.only(right: 20, top: 2),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                  child: SizedBox(
                    child: TextField(
                        controller: password,
                        obscureText: true,
                        decoration: new InputDecoration(
                          enabledBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 2.0),
                          ),
                          border: const OutlineInputBorder(),
                          labelStyle: TextStyle(color: Colors.brown),
                        )),
                    height: 50,
                  ),
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                ),
                Container(
                  child: Align(
                    alignment: Alignment.topRight,
                    child: SizedBox(
                      child: Text(
                        " Total capacity ",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ),
                  margin: EdgeInsets.only(right: 20, top: 2),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                  child: SizedBox(
                    child: TextField(
                        controller: txtAllCapacity,
                        decoration: new InputDecoration(
                          enabledBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.grey, width: 2.0),
                          ),
                          border: const OutlineInputBorder(),
                          //    hintText: 'mohmed',
                          labelStyle: TextStyle(color: Colors.brown),
                        )),
                    height: 50,
                  ),
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                ),
                Container(
                  child: Align(
                    alignment: Alignment.topRight,
                    child: SizedBox(
                      child: Text(
                        "Type",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ),
                  margin: EdgeInsets.only(right: 20, top: 20),
                ),
                Container(
                  width: 300,
                  child: DropdownButton(
                      icon: Icon(null),
                      iconSize: 30,
                      value: _value,
                      items: [
                        DropdownMenuItem(child: Text("Resturant"), value: 1),
                        DropdownMenuItem(child: Text("Cafe"), value: 2),
                        DropdownMenuItem(child: Text("salon"), value: 3),
                      ],
                      onChanged: (value) {
                        setState(() {
                          _value = value;
                        });
                      }),
                ),


                Container(
                  child: Align(
                    alignment: Alignment.topRight,
                    child: SizedBox(
                      child: Text(
                        "Statues",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ),
                  margin: EdgeInsets.only(right: 20, top: 20),
                ),
                Container(
                  width: 300,
                  child: DropdownButton(
                      icon: Icon(null),
                      iconSize: 30,
                      value: _value2,
                      items: [
                        DropdownMenuItem(child: Text("Extreme"), value: "Extreme"),
                        DropdownMenuItem(child: Text("High"), value: "High"),
                        DropdownMenuItem(child: Text("Medium"), value: "Medium"),
                        DropdownMenuItem(child: Text("Low"), value: "Low"),
                      ],
                      onChanged: (value) {
                        setState(() {
                          _value = value;
                        });
                      }),
                ),



                Container(
                  child: SizedBox(
                    child: RaisedButton(
                        child: progressSave
                            ? Center(
                                child: CircularProgressIndicator(
                                  backgroundColor: Colors.white,
                                ),
                              )
                            : Text(
                                "Register a new account",
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white),
                              ),
                        padding: EdgeInsets.fromLTRB(89, 0, 89, 0),
                        onPressed: _onRegisterPressed,
                        color: Theme.of(context).primaryColor,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0))),
                    height: 50,
                  ),
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 50),
                ),
              ],
            ))
          ],
        ),
      ),
    );
  }

  _onRegisterPressed() async {
    setState(() {
      progressSave = true;
    });

    DioUtil.createDio()
        .post("Main/Register",
            data: new Shops(
                    allCapacity: int.parse(txtAllCapacity.text),
                    currentCapacity: int.parse(txtAllCapacity.text),
                    id: 0,
                    statuse:  _value2,
                    password: password.text,
                    shopName: txtShopName.text,shopImage: txtimage.text,
                    type: _value,
                    username: txtUserName.text)
                .toJson())
        .then((response) {
      if (response.statusCode == 200) {
        if (response.data == "1") {
          Dialogs.okAlter(context, 'Register', '  Register Done');
          Navigator.pop(context);
        } else {
          Dialogs.okAlter(
              context, 'Register', 'Model Error ');
          setState(() {
            print(response.statusCode);
            print(response.data.toString());
            progressSave = false;
          });
        }
      } else {
        setState(() {
          progressSave = false;
        });
        print(
            'Error*Case********************************************************' +
                response.statusCode.toString());
        setState(() {});
      }
    }).catchError((error) {
      setState(() {
        progressSave = false;
      });

      print('Error*********************************************************' +
          error.toString());
    });
  }
}

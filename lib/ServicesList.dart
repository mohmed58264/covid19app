import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mwaedy_app/Detilse.dart';
import 'package:mwaedy_app/Shops.dart';

class ServicesList extends StatefulWidget {
 Shops shops;

  ServicesList(this.shops);

  @override
  _ServicesListState createState() => _ServicesListState();
}

class _ServicesListState extends State<ServicesList> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
      {
         Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Detailes(widget.shops.username))),
//print(widget.sbcvmv.services.serviceImages)
      },
      child: Container(
        child: Card(
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0)),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Stack(
                children: [
                  Container(
                    child: ClipRRect(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            topLeft: Radius.circular(10)),
                        child: Image.network(widget.shops.shopImage,
                            fit: BoxFit.fill,
                            height: 83,
                            width: 150,
                            loadingBuilder: (BuildContext context, Widget child,
                                ImageChunkEvent loadingProgress) {
                              if (loadingProgress == null) return child;
                              return Center(
                                child: CircularProgressIndicator(
                                  value: loadingProgress.expectedTotalBytes !=
                                      null
                                      ? loadingProgress.cumulativeBytesLoaded /
                                      loadingProgress.expectedTotalBytes
                                      : null,
                                ),
                              );
                            }
                        )),
                  ),

                ],
              ),
              Container(
                alignment: Alignment.topRight,
                width: 160,
                padding: EdgeInsets.only(right: 5, left: 5),
                child: Text(widget.shops.shopName,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Row(
                  children: [
                    Text(widget.shops.currentCapacity.toString(),
                      style: TextStyle(
                          color: Theme
                              .of(context)
                              .primaryColor,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(" Available number "),
                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class Dialogs{
    static  GlobalKey<ScaffoldState> ScaffoldStates=new GlobalKey();
    static int id=-1;

    static snackBar(BuildContext context,String str){
        ScaffoldStates.currentState.showSnackBar(new SnackBar(content: new Text(str),backgroundColor: Theme.of(context).accentColor,));
    }


    static isEmpty1(BuildContext context,String str)
    {
        if(str.isEmpty) {
            ScaffoldStates.currentState.showSnackBar(new SnackBar(duration: Duration(seconds: 2), content: new Text('لا يمكن ترك حقل فارغ'),backgroundColor: Theme.of(context).accentColor,),);
            return 0;
        }
    }

    static  isEmpty2(BuildContext context,String str,String str1)
    {
        if(str.isEmpty|| str1.isEmpty) {
            ScaffoldStates.currentState.showSnackBar(new SnackBar(content: new Text('لا يمكن ترك حقل فارغ'),backgroundColor: Colors.brown,));
            return 0;
        }
    }



    static  isEmpty3(BuildContext context,String str,String str1,String str2)
    {
        if(str.isEmpty|| str1.isEmpty || str2.isEmpty) {
            ScaffoldStates.currentState.showSnackBar(new SnackBar(content: new Text('لا يمكن ترك حقل فارغ'),backgroundColor:Theme.of(context).primaryColor,));
            return 0;
        }
    }


    static  isEmpty4(BuildContext context,String str,String str1,String str2,String str3)
    {
        if(str.isEmpty|| str1.isEmpty || str2.isEmpty|| str3.isEmpty) {
            ScaffoldStates.currentState.showSnackBar(new SnackBar(content: new Text('لا يمكن ترك حقل فارغ'),backgroundColor:Theme.of(context).primaryColor,));
            return 0;
        }
    }


    static  isEmpty5(String str,String str1,String str2,String str3,String str4)
    {
        if(str.isEmpty|| str1.isEmpty || str2.isEmpty|| str3.isEmpty|| str4.isEmpty) {
            ScaffoldStates.currentState.showSnackBar(new SnackBar(content: new Text('لا يمكن ترك حقل فارغ'),backgroundColor: Colors.brown,));
            return 0;
        }
    }




    static  Future<void> okAlter(BuildContext context,String title,String contain) {
        return showDialog<void>(
            context: context,
            builder: (BuildContext context) {
                return AlertDialog(
                    title: Text(title),
                    content: Text(contain),
                    actions: <Widget>[
                        FlatButton(
                            child: Text('موافق'),
                            onPressed: () {
                                Navigator.of(context).pop();
                            },
                        ),
                    ],
                );
            },
        );
    }
    /*********************************************************************************************/
    static  Future<void> progressbar(BuildContext context) {

        return showDialog<void>(
            barrierDismissible: false, // JUST MENTION THIS LINE
            context: context,
            builder: (BuildContext context) {
                return AlertDialog(
                    title: Text('الاتصال'),
                    content: Text('يتم الاتصال'),
                    actions: <Widget>[

                        new Container(padding: EdgeInsets.fromLTRB(110, 0, 0, 0),
                            child: CircularProgressIndicator()),

//             FlatButton(
//               child: Text('موافق'),
//               onPressed: () {
//                 Navigator.of(context).pop();
//               },
//             ),
                    ],
                );
            },
        );
    }
/**************************************************************************************************************/
    // enum ConfirmAction { CANCEL, ACCEPT }

    static asyncConfirmDialog(BuildContext context,String title,String contain) {
        showDialog(
            context: context,
            barrierDismissible: false, // user must tap button for close dialog!
            builder: (BuildContext context) {
                return AlertDialog(
                    title: Text(title),
                    content:  Text(contain),
                    actions: <Widget>[



                        FlatButton(
                            child: const Text('غير موافق'),
                            onPressed: () {
                                Navigator.of(context).pop();
                                id=0;
                            },
                        ),
                        FlatButton(
                            child: const Text('موافق'),
                            onPressed: () {
                                Navigator.of(context).pop();
                                id=1;
                            },
                        )



                    ],
                );
            },
        );
    }
    static asyncOkDialog(BuildContext context,String title,String contain) {
        showDialog(
            context: context,
            barrierDismissible: false, // user must tap button for close dialog!
            builder: (BuildContext context) {
                return AlertDialog(
                    title: Text(title),
                    content:  Text(contain),
                    actions: <Widget>[
                        FlatButton(
                            child: const Text('موافق'),
                            onPressed: () {
                                Navigator.of(context).pop();
                            },
                        )



                    ],
                );
            },
        );
    }


//   enum ConfirmAction { CANCEL, ACCEPT }
////https://androidkt.com/flutter-alertdialog-example/   : site for all type dialog
//   Future<ConfirmAction> _asyncConfirmDialog(BuildContext context) async {
//
//     return showDialog<ConfirmAction>(
//       context: context,
//       barrierDismissible: false, // user must tap button for close dialog!
//       builder: (BuildContext context) {
//         return AlertDialog(
//           title: Text('Reset settings?'),
//           content: const Text(
//               'This will reset your device to its default factory settings.'),
//           actions: <Widget>[
//             FlatButton(
//               child: const Text('CANCEL'),
//               onPressed: () {
//                 Navigator.of(context).pop(ConfirmAction.CANCEL);
//               },
//             ),
//             FlatButton(
//               child: const Text('ACCEPT'),
//               onPressed: () {
//                 Navigator.of(context).pop(ConfirmAction.ACCEPT);
//               },
//             )
//           ],
//         );
//       },
//     );
//   }
}
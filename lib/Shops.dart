class Shops {
  int id;
  String shopName;
  int allCapacity;
  int currentCapacity;
  int type;
  String shopImage;
  String username;
  String password;
  String statuse;

  Shops(
      {this.id,
        this.shopName,
        this.allCapacity,
        this.currentCapacity,
        this.type,
        this.statuse,
        this.shopImage,
        this.username,
        this.password});

  Shops.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    shopName = json['shopName'];
    allCapacity = json['allCapacity'];
    currentCapacity = json['currentCapacity'];
    type = json['type'];
    statuse = json['statuse'];
    shopImage = json['shopImage'];
    username = json['username'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statuse'] = this.statuse;
    data['id'] = this.id;
    data['shopName'] = this.shopName;
    data['allCapacity'] = this.allCapacity;
    data['currentCapacity'] = this.currentCapacity;
    data['type'] = this.type;
    data['shopImage'] = this.shopImage;
    data['username'] = this.username;
    data['password'] = this.password;
    return data;
  }
}
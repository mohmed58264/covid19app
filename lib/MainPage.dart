import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mwaedy_app/About.dart';
import 'package:mwaedy_app/Login.dart';
import 'package:mwaedy_app/ShopsPage.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentIndex = 0;
  double balance;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> bottomWidget = [
      ShopsPage(),
      AboutPage(),
    ];

    return Scaffold(
        key: _scaffoldKey,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(80.0),
            child: Container(
              child: AppBar(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)),
                centerTitle: true,
                title: Text("Stay away"),

                elevation: 0,
              ),
            )),
        body: Center(child: bottomWidget.elementAt(currentIndex)),
        bottomNavigationBar: bottomNav());
  }

  Widget bottomNav() {
    return BottomNavyBar(
      selectedIndex: currentIndex,
      showElevation: true,
      itemCornerRadius: 80,
      curve: Curves.ease,
      onItemSelected: (index) => setState(() {
        setState(() {
          currentIndex = index;
        });
      }),
      items: [
        BottomNavyBarItem(
          icon: Icon(Icons.home),
          title: Text('Shops'),
          activeColor: Colors.lightBlue,
          textAlign: TextAlign.center,
        ),
        BottomNavyBarItem(
          icon: Icon(Icons.info_outline),
          title: Text('About '),
          activeColor: Colors.purpleAccent,
          textAlign: TextAlign.center,
        ),

      ],
    );
  }
}

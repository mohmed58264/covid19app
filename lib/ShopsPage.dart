import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mwaedy_app/DioUtil.dart';
import 'package:mwaedy_app/ServicesList.dart';
import 'package:mwaedy_app/Shops.dart';

class ShopsPage extends StatefulWidget {
  @override
  _ShopsPageState createState() => _ShopsPageState();
}

class _ShopsPageState extends State<ShopsPage> {
  List<Shops> _list_shops_resturant;
  List<Shops> _list_shops__coffe;
  List<Shops> _list_shops_salone;
  bool progress = true;

  @override
  void initState() {
    _getResturant();
    _getCoffe();
    _getSalone();
    super.initState();
  }


  Widget _buildResturantItem(BuildContext context, int index) {
    return ServicesList(_list_shops_resturant.elementAt(index));
  }

  Widget _buildCoffeItem(BuildContext context, int index) {
    return ServicesList(_list_shops__coffe.elementAt(index));
  }


  Widget _buildSaloneItem(BuildContext context, int index) {
    return ServicesList(_list_shops_salone.elementAt(index));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(body:progress? Center():ListView(children: [


      Container(
        margin: EdgeInsets.fromLTRB(20, 30, 0, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(""),
            Spacer(),
            Container(
              child: Container(
                child: Text(
                  " | ",
                  style: TextStyle(
                      fontSize: 32,
                      color: Theme
                          .of(context)
                          .accentColor),
                ),
              ),
            ),
            Text(
              "Restaurants",
              style: TextStyle(color: Colors.black, fontSize: 20),
            ),
          ],
        ),
      ),
      Container(
        child: SizedBox(
          child: ListView.separated(
            itemBuilder: _buildResturantItem,
            scrollDirection: Axis.horizontal,
            separatorBuilder: (context, index) {
              return SizedBox(
                width: 0,
              );
            },
            itemCount: _list_shops_resturant.length ,

            shrinkWrap: true,
          ),
          height: 160,
        ),
      ),



      Container(
        margin: EdgeInsets.fromLTRB(20, 30, 0, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(""),
            Spacer(),
            Container(
              child: Container(
                child: Text(
                  " | ",
                  style: TextStyle(
                      fontSize: 32,
                      color: Theme
                          .of(context)
                          .accentColor),
                ),
              ),
            ),
            Text(
              "Cafes  ",
              style: TextStyle(color: Colors.black, fontSize: 20),
            ),
          ],
        ),
      ),
      Container(
        child: SizedBox(
          child: ListView.separated(
            itemBuilder: _buildCoffeItem,
            scrollDirection: Axis.horizontal,
            separatorBuilder: (context, index) {
              return SizedBox(
                width: 0,
              );
            },
            itemCount: _list_shops__coffe.length ,

            shrinkWrap: true,
          ),
          height: 160,
        ),
      ),



      Container(
        margin: EdgeInsets.fromLTRB(20, 30, 0, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(""),
            Spacer(),
            Container(
              child: Container(
                child: Text(
                  " | ",
                  style: TextStyle(
                      fontSize: 32,
                      color: Theme
                          .of(context)
                          .accentColor),
                ),
              ),
            ),
            Text(
              "Salons  ",
              style: TextStyle(color: Colors.black, fontSize: 20),
            ),
          ],
        ),
      ),
      Container(
        child: SizedBox(
          child: ListView.separated(
            itemBuilder: _buildSaloneItem,
            scrollDirection: Axis.horizontal,
            separatorBuilder: (context, index) {
              return SizedBox(
                width: 0,
              );
            },
            itemCount: _list_shops_salone.length,

            shrinkWrap: true,
          ),
          height: 160,
        ),
      ),
    ],),);
  }

  void _getResturant() async {
    progress = true;

    DioUtil.createDio().get("main/GetAllShops?type=1").then((response) {
      if (response.statusCode == 200) {
        var list = response.data as List;
        _list_shops_resturant = list.map((model) => Shops.fromJson(model)).toList();
          print(response.data);
        setState(() {
          print(response.data);
          progress = false;
        });



        setState(() {
          progress = false;
        });
      } else {
        setState(() {
          progress = false;
        });

        print(
            'Error*Case********************************************************' +
                response.statusCode.toString());
      }
    }).catchError((error) {
      setState(() {
        progress = false;
      });

      print('Error*********************************************************' +
          error.toString());
    });
  }

  void _getCoffe() async {
    progress = true;

    DioUtil.createDio().get("main/GetAllShops?type=2").then((response) {
      if (response.statusCode == 200) {
        var list = response.data as List;
        _list_shops__coffe = list.map((model) => Shops.fromJson(model)).toList();

        setState(() {
          print(response.data);
          progress = false;
        });



        setState(() {
          progress = false;
        });
      } else {
        setState(() {
          progress = false;
        });

        print(
            'Error*Case********************************************************' +
                response.statusCode.toString());
      }
    }).catchError((error) {
      setState(() {
        progress = false;
      });

      print('Error*********************************************************' +
          error.toString());
    });
  }


  void _getSalone() async {
    progress = true;

    DioUtil.createDio().get("main/GetAllShops?type=3").then((response) {
      if (response.statusCode == 200) {
        var list = response.data as List;
        _list_shops_salone = list.map((model) => Shops.fromJson(model)).toList();

        setState(() {
          print(response.data);
          progress = false;
        });



        setState(() {
          progress = false;
        });
      } else {
        setState(() {
          progress = false;
        });

        print(
            'Error*Case********************************************************' +
                response.statusCode.toString());
      }
    }).catchError((error) {
      setState(() {
        progress = false;
      });

      print('Error*********************************************************' +
          error.toString());
    });
  }

}

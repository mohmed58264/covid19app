import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mwaedy_app/Dialogs.dart';
import 'package:mwaedy_app/DioUtil.dart';
import 'package:mwaedy_app/Register.dart';
import 'package:mwaedy_app/UpdateShop.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  bool progress = false;
  TextEditingController username = new TextEditingController();
  TextEditingController password = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(key: Dialogs.ScaffoldStates,
      body:progress?Center(child: CircularProgressIndicator(),): Center(
        child: ListView(
          children: <Widget>[
            new Column(
              children: <Widget>[
                Container(
                  child: Image(
                    image: new AssetImage('assets/images/login.png'),
                    width: 150,
                    height: 150,
                  ),
                  margin: EdgeInsets.fromLTRB(0, 150, 0, 50),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                  child: SizedBox(
                    child: TextField(
                       controller: username,
                        textAlign: TextAlign.right,
                        decoration:
                            new InputDecoration(hintText: "Username")),
                    height: 50,
                  ),
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(1, 10, 1, 10),
                  child: SizedBox(
                    child: TextField(
                        controller: password,
                        textAlign: TextAlign.right,
                        obscureText: true,
                        decoration:
                            new InputDecoration(hintText: "Password")),
                    height: 50,
                  ),
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                ),
                Container(
                  margin: EdgeInsets.only(top: 80),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.all(5),
                        child: SizedBox(
                          width: 150,
                          child: OutlineButton(
                              child: Text(
                                "Register",
                                style: TextStyle(
                                    fontSize: 14, color: Colors.black),
                              ),
                              borderSide: BorderSide(
                                  color: Theme.of(context).primaryColor,
                                  width: 2,
                                  style: BorderStyle.solid),
                              onPressed: _onRegisterPressed,
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(10.0))),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(5),
                        child: SizedBox(
                          width: 150,
                          child: RaisedButton(
                              child: Text(
                                " Login ",
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white),
                              ),
                              onPressed: _onLoginPressed,
                              color: Theme.of(context).primaryColor,
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(10.0))),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  _onRegisterPressed() {

    Navigator.pop(context);

  Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => RegisterPage()),
    );


  }

  _onLoginPressed() {
    _login();
  }

  void _login() {
    setState(() {
      progress = true;
    });
    DioUtil.createDio()
        .get("Main/Login?username=" +
            username.text +
            "&password=" +
            password.text)
        .then((response) {
      if (response.statusCode == 200) {
        setState(() {
          if (response.data == "1") {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => UpdateShop(username.text)),
            );

          } else {
            Dialogs.snackBar(context, "Error Username Or Password");
          }
          progress = false;
        });
      } else {
        setState(() {
          progress = false;
        });
        print(
            'Error*Case********************************************************' +
                response.statusCode.toString());
      }
    }).catchError((error) {
      setState(() {
        progress = false;
      });

      print('Error*********************************************************' +
          error.toString());
    });
  }
}

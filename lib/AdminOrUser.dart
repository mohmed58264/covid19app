import 'dart:ui';
import 'package:mwaedy_app/Login.dart';
import 'package:mwaedy_app/MainPage.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AdminOrUser extends StatefulWidget {

  @override
  _AdminOrUserState createState() => _AdminOrUserState();
}

class _AdminOrUserState extends State<AdminOrUser> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ListView(
          children: <Widget>[
            new Column(
              children: <Widget>[
                Container(
                  child: Image(
                    image: new AssetImage('assets/images/logo.png'),
                    width: 150,
                    height: 150,
                  ),
                  margin: EdgeInsets.fromLTRB(0, 150, 0, 0),
                ),

                Container(
                  child: SizedBox(
                    child: OutlineButton(
                        child: Text(
                          "User",
                          style: TextStyle(fontSize: 14, color: Colors.black),
                        ),
                        padding: EdgeInsets.fromLTRB(100, 0, 100, 0),
                        borderSide: BorderSide(
                            color: Theme.of(context).primaryColor,
                            width: 2,
                            style: BorderStyle.solid),
                        onPressed: _onVisitorPressed,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0))),
                  ),
                  margin: EdgeInsets.fromLTRB(0, 180, 0, 0),
                ),
                Container(
                  child: SizedBox(
                    child: RaisedButton(
                        child: Text(
                          "Shopkeeper",
                          style: TextStyle(fontSize: 14, color: Colors.white),
                        ),
                        padding: EdgeInsets.fromLTRB(89, 0, 89, 0),
                        onPressed: _onLoginPressed,
                        color: Theme.of(context).primaryColor,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0))),
                  ),
                  margin: EdgeInsets.fromLTRB(0, 1, 0, 0),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  _onLoginPressed() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  _onVisitorPressed() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MainPage()),
    );
  }


}
